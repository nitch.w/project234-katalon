import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://34.238.150.232:8090/')

WebUI.setText(findTestObject('Object Repository/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/input_Password_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.waitForPageLoad(2)

WebUI.click(findTestObject('Object Repository/a_Total Transaction'))

WebUI.waitForPageLoad(1)

WebUI.verifyElementText(findTestObject('Object Repository/td_1'), '1')

WebUI.verifyElementText(findTestObject('Object Repository/td_Garden Papaya'), 'Garden, Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/td_2'), '2')

WebUI.verifyElementText(findTestObject('Object Repository/td_Banana Garden Banana Rambutan'), 'Banana, Garden, Banana, Rambutan')

WebDriver driver = DriverFactory.getWebDriver()


WebElement Table = driver.findElement(By.xpath('//*[@id="add-row"]/div/table/tbody'))


List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())


WebUI.verifyEqual(2, Rows.size())

WebUI.closeBrowser()

