## Group name

Fus Ro Dah

## Group Members

- 612115001 Chanagorn Kaewsootti
- 612115008 Nitchanun Wannayot

## Email addresses

- namuh.q@gmail.com (Nitchanun)
- champ.kaewsootti@gmail.com (Chanagorn)

## URL to webpage

http://34.238.150.232:8090/

## ip:port for backend

34.238.150.232:8090

## Deploy backend repository link

https://gitlab.com/se234-612115008/project-backend
