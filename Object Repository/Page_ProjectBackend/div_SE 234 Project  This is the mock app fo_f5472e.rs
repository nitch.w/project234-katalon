<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_SE 234 Project  This is the mock app fo_f5472e</name>
   <tag></tag>
   <elementGuidId>ffd5df26-017e-4f1e-8236-b101d9605f6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//app-top-banner/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jumbotron</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  SE 234 Project
  This is the mock app for the SE 234 project
  
  
    
      
        
          Products
          
        
        
          Carts
            1
          
        
        
      
      Logout
    
  
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/div[@class=&quot;row&quot;]/app-top-banner[@class=&quot;col-12&quot;]/div[@class=&quot;jumbotron&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-top-banner/div</value>
   </webElementXpaths>
</WebElementEntity>
