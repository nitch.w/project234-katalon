<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test - Cart page (User)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>04936c91-8abb-4493-83a7-2cc33341ec75</testSuiteGuid>
   <testCaseLink>
      <guid>217c32dd-4636-48fd-9e10-02ab7d9a70e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Carts page (User)/Amount, total price and confirm button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcdfe65e-51c4-4cff-a203-5189b72bf7e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Carts page (User)/Carts are present (when clicks add to chart btn)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
